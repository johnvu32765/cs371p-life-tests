*** Life<FredkinCell> 8x7 ***

Generation = 0, Population = 10.
0------
-------
------0
-----0-
-----0-
0--0--0
0-0---0
-------

Generation = 8, Population = 19.
3-0---0
1--1--3
4-0--22
0--0-33
-----30
-----0-
1-----6
-------

Generation = 16, Population = 15.
4-----6
-----2-
4----6-
1--0--4
5-0----
-----5-
-----9-
2--1--6

Generation = 24, Population = 21.
-----9-
4--3-58
7-3--+-
4--3-65
--2--8-
8--4-76
-----+7
-----3-

Generation = 32, Population = 14.
-----+7
5--3--9
--5---7
-------
+------
-------
7-2----
5--3-6+

Generation = 40, Population = 18.
+-2----
8--6-8+
--5----
-------
+-3---4
+--5--+
--4--+-
6--3--+

Generation = 48, Population = 19.
--4--+-
-------
+----++
+--4--+
+-----+
+--8-++
+------
9--6-9+

Generation = 56, Population = 10.
+------
-------
------+
-----+-
-----+-
+--8--+
+-5---+
-------

Generation = 64, Population = 19.
+-5---+
+--7--+
+-6--++
+--6-++
-----++
-----+-
+-----+
-------

Generation = 72, Population = 15.
+-----+
-----+-
+----+-
+--6--+
+-5----
-----+-
-----+-
+--7--+

Generation = 80, Population = 21.
-----+-
+--9-++
+-9--+-
+--9-++
--7--+-
+--+-++
-----++
-----+-

Generation = 88, Population = 14.
-----++
+--9--+
--+---+
-------
+------
-------
+-7----
+--9-++

Generation = 96, Population = 18.
+-7----
+--+-++
--+----
-------
+-8---+
+--+--+
--9--+-
+--9--+

Generation = 104, Population = 19.
--9--+-
-------
+----++
+--+--+
+-----+
+--+-++
+------
+--+-++

Generation = 112, Population = 10.
+------
-------
------+
-----+-
-----+-
+--+--+
+-+---+
-------

Generation = 120, Population = 19.
+-+---+
+--+--+
+-+--++
+--+-++
-----++
-----+-
+-----+
-------

Generation = 128, Population = 15.
+-----+
-----+-
+----+-
+--+--+
+-+----
-----+-
-----+-
+--+--+

Generation = 136, Population = 21.
-----+-
+--+-++
+-+--+-
+--+-++
--+--+-
+--+-++
-----++
-----+-

Generation = 144, Population = 14.
-----++
+--+--+
--+---+
-------
+------
-------
+-+----
+--+-++

Generation = 152, Population = 18.
+-+----
+--+-++
--+----
-------
+-+---+
+--+--+
--+--+-
+--+--+

Generation = 160, Population = 19.
--+--+-
-------
+----++
+--+--+
+-----+
+--+-++
+------
+--+-++

Generation = 168, Population = 10.
+------
-------
------+
-----+-
-----+-
+--+--+
+-+---+
-------

Generation = 176, Population = 19.
+-+---+
+--+--+
+-+--++
+--+-++
-----++
-----+-
+-----+
-------

Generation = 184, Population = 15.
+-----+
-----+-
+----+-
+--+--+
+-+----
-----+-
-----+-
+--+--+

Generation = 192, Population = 21.
-----+-
+--+-++
+-+--+-
+--+-++
--+--+-
+--+-++
-----++
-----+-

Generation = 200, Population = 14.
-----++
+--+--+
--+---+
-------
+------
-------
+-+----
+--+-++

Generation = 208, Population = 18.
+-+----
+--+-++
--+----
-------
+-+---+
+--+--+
--+--+-
+--+--+

Generation = 216, Population = 19.
--+--+-
-------
+----++
+--+--+
+-----+
+--+-++
+------
+--+-++

Generation = 224, Population = 10.
+------
-------
------+
-----+-
-----+-
+--+--+
+-+---+
-------

Generation = 232, Population = 19.
+-+---+
+--+--+
+-+--++
+--+-++
-----++
-----+-
+-----+
-------

Generation = 240, Population = 15.
+-----+
-----+-
+----+-
+--+--+
+-+----
-----+-
-----+-
+--+--+

*** Life<FredkinCell> 10x2 ***

Generation = 0, Population = 8.
-0
-0
0-
-0
-0
-0
--
--
-0
-0

*** Life<FredkinCell> 1x5 ***

Generation = 0, Population = 5.
00000

Generation = 38, Population = 2.
-0-0-

Generation = 76, Population = 2.
-0-0-

Generation = 114, Population = 2.
-0-0-

Generation = 152, Population = 2.
-0-0-

*** Life<FredkinCell> 10x8 ***

Generation = 0, Population = 4.
--------
--------
--------
--------
--------
--0----0
-----0--
--------
-0------
--------

Generation = 60, Population = 33.
-----+-+
7+++++-+
---8---+
87+--++-
-+---+--
++---+++
---+----
+-+-8+--
-----+--
5-+-++--

Generation = 120, Population = 34.
-+-----+
-+-+---+
---+-+--
++-+++--
-+------
++---+++
---+-+--
-++-+-++
-+-+---+
+-++-+++

Generation = 180, Population = 25.
-+------
---++-++
--------
-+-+---+
-+-+-+--
--+----+
-----+-+
+-+-++--
-+-----+
+---++-+

*** Life<FredkinCell> 8x1 ***

Generation = 0, Population = 6.
0
0
0
0
0
-
0
-

Generation = 16, Population = 4.
-
2
-
2
-
6
2
-

Generation = 32, Population = 4.
-
4
-
-
5
+
-
5

Generation = 48, Population = 3.
-
-
+
-
-
+
7
-

Generation = 64, Population = 3.
9
-
-
9
-
-
-
+

*** Life<FredkinCell> 10x2 ***

Generation = 0, Population = 16.
-0
00
00
0-
00
0-
00
00
00
0-

Generation = 67, Population = 6.
--
-+
-+
+-
--
+-
--
--
++
--

*** Life<FredkinCell> 9x10 ***

Generation = 0, Population = 19.
0-0--0--0-
----0-----
-0--------
--00------
---0-----0
----0---0-
0-00-0--0-
----------
-0--0-----

Generation = 21, Population = 44.
93-57--981
-02-40-14-
561--8-1--
86--7--564
+3----4-9-
-12---7---
1--4-3-9-8
4--6----33
5---14-6--

Generation = 42, Population = 49.
------+-++
-3857-6-+5
+-4-9+8---
-++++7++++
---79---++
+35984-5--
6+----9+7+
--9+98--+-
-8---+-+--

Generation = 63, Population = 44.
++8+-+----
+8+8+--6-8
-+-++---+-
-++--+----
-++-+++-+-
--+--7+8+-
---+7-+--+
-++-+--+--
++--++--++

Generation = 84, Population = 46.
+---++-+-+
+-+-+-+-+-
--+-++++++
+-----++--
-++-++-++-
+-------+-
++-+---+++
++-+--++++
+-+----+++

Generation = 105, Population = 40.
---+++--+-
+---+-+--+
--++++++--
--+++--+-+
+---++-+--
-+---9++-+
---+---++-
--+++-----
++----++++

Generation = 126, Population = 41.
----++---+
-+---+-++-
--+------+
+---++++-+
+--++--+-+
-++-+-----
---+--++++
+-++++-+++
+----+-+++

Generation = 147, Population = 46.
++++--+--+
-+--+--+++
-++-+++++-
-+--+--+--
+-+--++---
++-+-+++++
-++-+-+-++
-+--+-+---
-+++----+-

Generation = 168, Population = 39.
++---+++--
--+-+-+-+-
++-----++-
--++-++++-
-++--++--+
+---------
+-+-----+-
--+-++++++
-----++++-

Generation = 189, Population = 41.
-+---+++-+
--+--++--+
--++--+---
++--++-++-
+++-++--+-
--+-+----+
-+-+-++++-
-+----+-+-
+--++--+-+

Generation = 210, Population = 43.
----+----+
+-+-+---++
+-+--++-+-
+---+-++++
+---+---+-
-++-++-+++
++---++-+-
--+-++-+-+
+-++--++--

Generation = 231, Population = 46.
-++++--+-+
++---+----
+--++--+-+
-++-+-++++
+----+++--
-++--++---
++++--+-+-
---+-++--+
-++++--+++

Generation = 252, Population = 44.
--+--+-+--
+---+-+-+-
+++-+-+++-
++++++----
+-+-+-+--+
--+-+-+-+-
-++--+++--
--++-++++-
+----++--+

Generation = 273, Population = 39.
+-+-+-----
----+--+-+
-+++--+--+
--+-+--++-
+--+-+-+-+
-+++--+-++
-+--++-+-+
-+++----++
+---++----

*** Life<FredkinCell> 6x5 ***

Generation = 0, Population = 9.
0----
---00
-0--0
0--0-
-0---
-0---

Generation = 93, Population = 15.
+----
-+++-
-+++-
-+-++
+-+++
---+-

*** Life<FredkinCell> 2x2 ***

Generation = 0, Population = 4.
00
00

Generation = 77, Population = 0.
--
--

Generation = 154, Population = 0.
--
--

Generation = 231, Population = 0.
--
--

Generation = 308, Population = 0.
--
--

Generation = 385, Population = 0.
--
--

Generation = 462, Population = 0.
--
--

*** Life<FredkinCell> 4x9 ***

Generation = 0, Population = 1.
---------
-----0---
---------
---------

Generation = 75, Population = 11.
-----0-0-
----0---0
-0-0-0---
0---0-0-0

Generation = 150, Population = 5.
---------
-----0-0-
---------
-0-0---0-

Generation = 225, Population = 9.
-0---0---
0-0-0-0--
-0-------
----0-0--

Generation = 300, Population = 3.
---------
-0---0---
---------
-----0---

*** Life<FredkinCell> 7x4 ***

Generation = 0, Population = 14.
-00-
0---
-0--
000-
0000
00--
0---

Generation = 16, Population = 16.
4875
--0-
-4-3
36-6
4--4
-647
--5-

Generation = 32, Population = 14.
+--+
2-2-
---7
--5+
-4+-
8-8+
+-+-

Generation = 48, Population = 14.
-++-
4---
-+--
8+8-
+8+8
++--
+---

Generation = 64, Population = 16.
++++
--4-
-+-+
++-+
+--+
-+++
--+-

Generation = 80, Population = 14.
+--+
6-6-
---+
--++
-++-
+-++
+-+-

Generation = 96, Population = 14.
-++-
8---
-+--
+++-
++++
++--
+---

Generation = 112, Population = 16.
++++
--8-
-+-+
++-+
+--+
-+++
--+-

Generation = 128, Population = 14.
+--+
+-+-
---+
--++
-++-
+-++
+-+-

Generation = 144, Population = 14.
-++-
+---
-+--
+++-
++++
++--
+---

*** Life<FredkinCell> 4x8 ***

Generation = 0, Population = 5.
0-0-0---
----0---
--------
----0---

Generation = 66, Population = 7.
--------
+---+-+-
--+-+-+-
+-------

Generation = 132, Population = 7.
--+-----
--+-+---
--+-----
+---+-+-

Generation = 198, Population = 4.
+-----+-
------+-
--------
------+-

*** Life<FredkinCell> 2x5 ***

Generation = 0, Population = 8.
0000-
0-000

Generation = 96, Population = 3.
-----
2-0-2

Generation = 192, Population = 3.
-----
2-0-2

Generation = 288, Population = 3.
-----
2-0-2

*** Life<FredkinCell> 7x1 ***

Generation = 0, Population = 1.
-
-
0
-
-
-
-

Generation = 27, Population = 0.
-
-
-
-
-
-
-

Generation = 54, Population = 0.
-
-
-
-
-
-
-

*** Life<FredkinCell> 4x6 ***

Generation = 0, Population = 16.
00-000
00-00-
0-000-
-00-0-

Generation = 61, Population = 14.
+-+++-
-+++-+
-+-++-
++-+--

Generation = 122, Population = 13.
++-++-
--+---
--++++
-++++-

Generation = 183, Population = 11.
+--++-
-+-+-+
---+++
--++--

Generation = 244, Population = 11.
-+---+
--++++
++--++
-+----

Generation = 305, Population = 7.
----+-
----+-
---+--
++-+-+

Generation = 366, Population = 12.
--+--+
+-+-++
+--+-+
+++---

Generation = 427, Population = 15.
++--+-
+++++-
+++-++
+--+--

Generation = 488, Population = 13.
-+-+++
++--+-
+-++++
+-----

*** Life<FredkinCell> 8x10 ***

Generation = 0, Population = 9.
----0----0
----------
0----0----
---0-----0
--0-------
---0-----0
----------
----------

Generation = 59, Population = 37.
+-7++---+-
0+--0-09-+
----+9+7++
0---0+---8
---+---+-+
-+-+-+----
36---++---
-50+09--0-

Generation = 118, Population = 29.
--++++--+-
-+-+---+--
+--+--++--
-+---+---+
+-+-+--++-
-+-+-+-+--
-++-+--+--
-----+----

Generation = 177, Population = 43.
-+-++---++
-+--0+0--+
++++-++--+
0--+-+--0+
++-+-+-+++
0+-+0--+-+
-+--+++-+-
----0+---+

Generation = 236, Population = 26.
---+--++-+
-+-+-+----
+-++---++-
-+-+-+----
+--+-+--++
-+--------
-++--+---+
---------+

*** Life<FredkinCell> 4x2 ***

Generation = 0, Population = 8.
00
00
00
00

Generation = 6, Population = 8.
22
22
22
22

Generation = 12, Population = 8.
44
44
44
44

Generation = 18, Population = 8.
66
66
66
66

Generation = 24, Population = 8.
88
88
88
88

Generation = 30, Population = 8.
++
++
++
++

Generation = 36, Population = 8.
++
++
++
++

Generation = 42, Population = 8.
++
++
++
++

Generation = 48, Population = 8.
++
++
++
++

Generation = 54, Population = 8.
++
++
++
++

Generation = 60, Population = 8.
++
++
++
++

Generation = 66, Population = 8.
++
++
++
++

Generation = 72, Population = 8.
++
++
++
++

Generation = 78, Population = 8.
++
++
++
++

Generation = 84, Population = 8.
++
++
++
++

Generation = 90, Population = 8.
++
++
++
++

Generation = 96, Population = 8.
++
++
++
++

Generation = 102, Population = 8.
++
++
++
++

Generation = 108, Population = 8.
++
++
++
++

Generation = 114, Population = 8.
++
++
++
++

Generation = 120, Population = 8.
++
++
++
++

Generation = 126, Population = 8.
++
++
++
++

Generation = 132, Population = 8.
++
++
++
++

Generation = 138, Population = 8.
++
++
++
++

Generation = 144, Population = 8.
++
++
++
++

Generation = 150, Population = 8.
++
++
++
++

Generation = 156, Population = 8.
++
++
++
++

Generation = 162, Population = 8.
++
++
++
++

Generation = 168, Population = 8.
++
++
++
++

Generation = 174, Population = 8.
++
++
++
++

Generation = 180, Population = 8.
++
++
++
++

Generation = 186, Population = 8.
++
++
++
++

Generation = 192, Population = 8.
++
++
++
++

Generation = 198, Population = 8.
++
++
++
++

Generation = 204, Population = 8.
++
++
++
++

Generation = 210, Population = 8.
++
++
++
++

Generation = 216, Population = 8.
++
++
++
++

Generation = 222, Population = 8.
++
++
++
++

Generation = 228, Population = 8.
++
++
++
++

Generation = 234, Population = 8.
++
++
++
++

Generation = 240, Population = 8.
++
++
++
++

Generation = 246, Population = 8.
++
++
++
++

Generation = 252, Population = 8.
++
++
++
++

Generation = 258, Population = 8.
++
++
++
++

Generation = 264, Population = 8.
++
++
++
++

Generation = 270, Population = 8.
++
++
++
++

Generation = 276, Population = 8.
++
++
++
++

Generation = 282, Population = 8.
++
++
++
++

Generation = 288, Population = 8.
++
++
++
++

Generation = 294, Population = 8.
++
++
++
++

Generation = 300, Population = 8.
++
++
++
++

Generation = 306, Population = 8.
++
++
++
++

Generation = 312, Population = 8.
++
++
++
++

Generation = 318, Population = 8.
++
++
++
++

Generation = 324, Population = 8.
++
++
++
++

Generation = 330, Population = 8.
++
++
++
++

Generation = 336, Population = 8.
++
++
++
++

Generation = 342, Population = 8.
++
++
++
++

Generation = 348, Population = 8.
++
++
++
++

Generation = 354, Population = 8.
++
++
++
++

Generation = 360, Population = 8.
++
++
++
++

Generation = 366, Population = 8.
++
++
++
++

Generation = 372, Population = 8.
++
++
++
++

Generation = 378, Population = 8.
++
++
++
++

Generation = 384, Population = 8.
++
++
++
++

Generation = 390, Population = 8.
++
++
++
++

Generation = 396, Population = 8.
++
++
++
++

Generation = 402, Population = 8.
++
++
++
++

Generation = 408, Population = 8.
++
++
++
++

Generation = 414, Population = 8.
++
++
++
++

Generation = 420, Population = 8.
++
++
++
++

Generation = 426, Population = 8.
++
++
++
++

Generation = 432, Population = 8.
++
++
++
++

Generation = 438, Population = 8.
++
++
++
++

Generation = 444, Population = 8.
++
++
++
++

Generation = 450, Population = 8.
++
++
++
++

Generation = 456, Population = 8.
++
++
++
++

Generation = 462, Population = 8.
++
++
++
++

Generation = 468, Population = 8.
++
++
++
++

Generation = 474, Population = 8.
++
++
++
++

Generation = 480, Population = 8.
++
++
++
++

Generation = 486, Population = 8.
++
++
++
++

*** Life<FredkinCell> 5x6 ***

Generation = 0, Population = 16.
0-000-
---00-
000-0-
0--000
--00--

Generation = 46, Population = 21.
62++-+
6-7--5
9++-+9
-+-++-
+5-9+9

Generation = 92, Population = 12.
----+-
+++-+-
---+++
--+--+
--+-+-

Generation = 138, Population = 20.
---+-+
++---+
++++++
+-++++
+-++-+

Generation = 184, Population = 17.
+--+-+
-+++++
+--+--
-+--++
+--+++

Generation = 230, Population = 15.
-9-++-
+--++-
--+--+
--+-++
++-++-

*** Life<FredkinCell> 1x1 ***

Generation = 0, Population = 1.
0

Generation = 23, Population = 0.
-

Generation = 46, Population = 0.
-

*** Life<FredkinCell> 3x4 ***

Generation = 0, Population = 10.
0000
00-0
00-0

Generation = 38, Population = 6.
-+--
+++-
06--

Generation = 76, Population = 6.
0--+
--++
--0+

Generation = 114, Population = 8.
-+-+
++-+
-+0+

Generation = 152, Population = 8.
-+0-
+++-
0+0-

Generation = 190, Population = 6.
---+
--++
0-0+

Generation = 228, Population = 10.
0+0+
++-+
0+-+

Generation = 266, Population = 6.
-+--
+++-
0+--

Generation = 304, Population = 6.
0--+
--++
--0+

*** Life<FredkinCell> 10x5 ***

Generation = 0, Population = 18.
-0000
-00--
----0
---00
0----
000--
-0--0
----0
---0-
-0---

